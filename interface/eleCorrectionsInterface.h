#ifndef eleCorrectionsInterface_h
#define eleCorrectionsInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class eleCorrectionsInterface                                                                                //
//                                                                                                                //
//   Class to compute EGM corrections from correctionlib json.                                                         //
//                                                                                                                //
//   Author: Jona Motta (jona.motta@cern.ch)                                                                      //
//   Date  : December 2024                                                                                           //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries

//CMSSW libraries
#include "Base/Modules/interface/correctionWrapper.h"
#include <ROOT/RVec.hxx>
#include <TRandomGen.h>

// // libraries of deterministic seed production and RNG
#include "Base/Modules/interface/DeterministicSeedInterface.h"
#include "Base/Modules/interface/NumpySFC64GeneratorInterface.h"

typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<int> iRVec;
typedef ROOT::VecOps::RVec<uint64_t> ui64RVec;

struct egmSSOutput {
  ui64RVec egm_smear_seed;
  fRVec    egm_smear_factor;
};

class eleCorrections {
  public:
    eleCorrections(std::string filename, std::string corrname);
    ~eleCorrections();
    
    // function for Run2 scale factor
    fRVec get_ele_sf(std::string corrname, std::string syst, std::string wp,
                     fRVec eta, fRVec pt);
    
    // function for Run3 scale factor
    fRVec get_ele_sf(std::string corrname, std::string syst, std::string wp,
                     fRVec eta, fRVec pt, fRVec phi);

    // function for scale correction
    fRVec get_ele_ss(Double_t run, 
                     iRVec gain, fRVec eta, fRVec r9, fRVec pt);

    // function for scale correction uncertainty
    fRVec get_ele_scale_unc(Double_t run, std::string syst,
                            iRVec gain, fRVec eta, fRVec r9, fRVec pt);

    // function for smearing correction with non synched seed
    egmSSOutput get_ele_ss(int event, std::string syst,
                           fRVec eta, fRVec r9, fRVec pt);

    // function for smearing correction with synched seed
    egmSSOutput get_ele_synched_ss(uint64_t event_seed, std::string syst,
                                   fRVec eta, fRVec r9, fRVec pt);

  private:
    MyCorrections elecorr;
    TRandomMT64 rndm = TRandomMT64(7);
    NumpyGenerator RNGnumpy = NumpyGenerator("SFC64");
    DeterministicSeedInterface RNGseed = DeterministicSeedInterface(); 

};

#endif // eleCorrectionsInterface_h
