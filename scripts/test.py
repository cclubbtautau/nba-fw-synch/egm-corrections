import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.EGM.eleCorrections import eleSFRDF, eleSSRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"
get_test_file = lambda x: os.path.expandvars(os.path.join(test_folder, x))

import pandas as pd

import argparse
parser = argparse.ArgumentParser(description='Plotter options')
parser.add_argument('-s','--save', action='store_true', default=False,
    help="Stores test results in a pickle file")
parser.add_argument('-c','--check', action='store_true', default=False,
    help="Compares test results with previously stored results")
options = parser.parse_args()

def ele_sf_test_run2(df, isMC, year, isUL, runPeriod):
    elesf_UL16preVFP = eleSFRDF(
        isMC=isMC,
        year=year,
        isUL=isUL,
        runPeriod=runPeriod,
        wps=["wp80iso", "RecoBelow20", "RecoAbove20"]
    )()
    df, _ = elesf_UL16preVFP.run(df)
    h = df.Histo1D("elesf_wp80iso")
    print(f"Electron {year}{runPeriod} wp80iso SF Integral: %.3f, Mean: %.3f, Std: %.3f" %
            (h.Integral(), h.GetMean(), h.GetStdDev()))

    h1 = df.Histo1D("elesf_RecoBelow20")
    print(f"Electron {year}{runPeriod} RecoBelow20 SF Integral: %.3f, Mean: %.3f, Std: %.3f" %
            (h1.Integral(), h1.GetMean(), h1.GetStdDev()))

    h2 = df.Histo1D("elesf_RecoAbove20")
    print(f"Electron {year}{runPeriod} RecoAbove20 SF Integral: %.3f, Mean: %.3f, Std: %.3f" %
            (h2.Integral(), h2.GetMean(), h2.GetStdDev()))

    return df, (h.Integral(), h.GetMean(), h.GetStdDev(),
                h1.Integral(), h1.GetMean(), h1.GetStdDev(),
                h2.Integral(), h2.GetMean(), h2.GetStdDev())

def ele_sf_test_run3(df, isMC, year, runPeriod):
    if year < 2023:
        elesf = eleSFRDF(
            isMC=isMC,
            year=year,
            runPeriod=runPeriod,
            wps=["wp80iso", "RecoBelow20", "RecoAbove20"],
        )()
    else:
        elesf = eleSFRDF(
            isMC=isMC,
            year=year,
            runPeriod=runPeriod,
            wps=["wp80iso", "RecoBelow20", "RecoAbove20"],
            electron_syst="",
            electron_central=""
        )()

    df, _ = elesf.run(df)
    h = df.Histo1D("elesf_wp80iso")
    print(f"Electron {year}{runPeriod} wp80iso SF Integral: %.3f, Mean: %.3f, Std: %.3f" %
            (h.Integral(), h.GetMean(), h.GetStdDev()))

    h1 = df.Histo1D("elesf_RecoBelow20")
    print(f"Electron {year}{runPeriod} RecoBelow20 SF Integral: %.3f, Mean: %.3f, Std: %.3f" %
            (h1.Integral(), h1.GetMean(), h1.GetStdDev()))

    h2 = df.Histo1D("elesf_RecoAbove20")
    print(f"Electron {year}{runPeriod} RecoAbove20 SF Integral: %.3f, Mean: %.3f, Std: %.3f" %
            (h2.Integral(), h2.GetMean(), h2.GetStdDev()))

    return df, (h.Integral(), h.GetMean(), h.GetStdDev(),
                h1.Integral(), h1.GetMean(), h1.GetStdDev(),
                h2.Integral(), h2.GetMean(), h2.GetStdDev())

def ele_ss_test_run3(df, isMC, year, runPeriod):
    eless = eleSSRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod
    )()
    df, _ = eless.run(df)
    if isMC:
        h = df.Histo1D("eless_smear")
        print(f"Electron {year}{runPeriod} SS Integral: %.3f, Mean: %.3f, Std: %.3f" % 
                (h.Integral(), h.GetMean(), h.GetStdDev()))

        h = df.Histo1D("eless_scale_up")
        print(f"Electron {year}{runPeriod} SS_scale_up Integral: %.3f, Mean: %.3f, Std: %.3f" % 
                (h.Integral(), h.GetMean(), h.GetStdDev()))
    else:
        h = df.Histo1D("eless_scale")
        print(f"Electron {year}{runPeriod} SS Integral: %.3f, Mean: %.3f, Std: %.3f" % 
                (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df, (h.Integral(), h.GetMean(), h.GetStdDev())


if __name__ == "__main__":
    results = []

    df_mc2016preVFP = ROOT.RDataFrame("Events", get_test_file("testfile_mc2016.root"))
    _, res = ele_sf_test_run2(df_mc2016preVFP, True, 2016, True, "preVFP")
    results.append(("2016_preVFP", res[0], res[1], res[2]))
    results.append(("2016_preVFP RecoBelow20", res[3], res[4], res[5]))
    results.append(("2016_preVFP RecoAbove20", res[6], res[7], res[8]))

    df_mc2016postVFP = ROOT.RDataFrame("Events", get_test_file("testfile_mc2016_apv.root"))
    _, res = ele_sf_test_run2(df_mc2016postVFP, True, 2016, True, "postVFP")
    results.append(("2016_postVFP", res[0], res[1], res[2]))
    results.append(("2016_postVFP RecoBelow20", res[3], res[4], res[5]))
    results.append(("2016_postVFP RecoAbove20", res[6], res[7], res[8]))

    df_mc2017 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2017.root"))
    _, res = ele_sf_test_run2(df_mc2017, True, 2017, True, "")
    results.append(("2017", res[0], res[1], res[2]))
    results.append(("2017 RecoBelow20", res[3], res[4], res[5]))
    results.append(("2017 RecoAbove20", res[6], res[7], res[8]))

    df_mc2018 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2018.root"))
    _, res = ele_sf_test_run2(df_mc2018, True, 2018, True, "")
    results.append(("2018", res[0], res[1], res[2]))
    results.append(("2018 RecoBelow20", res[3], res[4], res[5]))
    results.append(("2018 RecoAbove20", res[6], res[7], res[8]))

    df_mc2022 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022.root"))
    _, res = ele_ss_test_run3(df_mc2022, True, 2022, "preEE")
    results.append(("2022_ss", res[0], res[1], res[2]))
    _, res = ele_sf_test_run3(_, True, 2022, "preEE")
    results.append(("2022", res[0], res[1], res[2]))
    results.append(("2022 RecoBelow20", res[3], res[4], res[5]))
    results.append(("2022 RecoAbove20", res[6], res[7], res[8]))

    df_mc2022_postEE = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022_postee.root"))
    _, res = ele_ss_test_run3(df_mc2022_postEE, True, 2022, "postEE")
    results.append(("2022_postEE_ss", res[0], res[1], res[2]))
    _, res = ele_sf_test_run3(_, True, 2022, "postEE")
    results.append(("2022_postEE", res[0], res[1], res[2]))
    results.append(("2022_postEE RecoBelow20", res[3], res[4], res[5]))
    results.append(("2022_postEE RecoAbove20", res[6], res[7], res[8]))

    df_data2022 = ROOT.RDataFrame("Events", get_test_file("testfile_data2022.root"))
    _, res = ele_ss_test_run3(df_data2022, False, 2022, "preEE")
    results.append(("2022_data_ss", res[0], res[1], res[2]))

    df_data2022_postEE = ROOT.RDataFrame("Events", get_test_file("testfile_data2022_postee.root"))
    _, res = ele_ss_test_run3(df_data2022_postEE, False, 2022, "postEE")
    results.append(("2022_postEE_data_ss", res[0], res[1], res[2]))

    df_mc2023 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023.root"))
    _, res = ele_ss_test_run3(df_mc2023, True, 2023, "preBPix")
    results.append(("2023_ss", res[0], res[1], res[2]))
    _, res = ele_sf_test_run3(df_mc2023, True, 2023, "preBPix")
    results.append(("2023", res[0], res[1], res[2]))
    results.append(("2023 RecoBelow20", res[3], res[4], res[5]))
    results.append(("2023 RecoAbove20", res[6], res[7], res[8]))

    df_mc2023_postBPix = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023_postbpix.root"))
    _, res = ele_ss_test_run3(df_mc2023_postBPix, True, 2023, "postBPix")
    results.append(("2023_postBPix_ss", res[0], res[1], res[2]))
    _, res = ele_sf_test_run3(df_mc2023_postBPix, True, 2023, "postBPix")
    results.append(("2023_postBPix", res[0], res[1], res[2]))
    results.append(("2023_postBPix RecoBelow20", res[3], res[4], res[5]))
    results.append(("2023_postBPix RecoAbove20", res[6], res[7], res[8]))

    pd_df = pd.DataFrame(results)

    results_path = os.path.expandvars("$CMSSW_BASE/src/Corrections/EGM/scripts/results.pickle")
    if options.save:
        pd_df.to_pickle(results_path)
    elif options.check:
        stored_df = pd.read_pickle(results_path)
        pd_matches_with_saved_df = pd_df.equals(stored_df)
        if not pd_matches_with_saved_df:
            raise ValueError("Test results obtained do not agree with the stored results. If this "
                "is expected, please execute locally python3 test.py -s to overwrite the "
                "stored results and upload the resulting file")

