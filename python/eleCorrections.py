import os
import envyaml

from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/EGM/python/eleCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class eleSFRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(eleSFRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.wps = kwargs.pop("wps")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix + year

        if self.isMC:
            if not isUL and self.year < 2022:
                raise ValueError("Only implemented for Run2 UL datasets")

            if not os.getenv("_essCorr"):
                os.environ["_essCorr"] = "_essCorr"
                if "/libCorrectionsEGM.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libCorrectionsEGM.so")
                base = "{}/src/Corrections/EGM".format(os.getenv("CMSSW_BASE"))
                ROOT.gROOT.ProcessLine(".L {}/interface/eleCorrectionsInterface.h".format(base))

            if not os.getenv(f"_eleSF_{self.corrKey}"):
                os.environ[f"_eleSF_{self.corrKey}"] = "_eleSF"
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_%s = eleCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SF"][self.corrKey]["fileName"],
                            corrCfg["SF"][self.corrKey]["corrName"]))

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for syst_name, syst in [("", "sf"), ("_up", "sfup"), ("_down", "sfdown")]:
            # store systematics impact on SF only for the central production;
            # skip them for all systematic variated selections
            if self.skip_unused_systs and self.systs != "" and syst != "sf":
                continue

            for wp in self.wps:
                if self.year <= 2022:
                    df = df.Define("elesf_%s%s" % (wp, syst_name),
                        'corr_%s.get_ele_sf("%s", "%s", "%s", Electron_eta, Electron_pt%s)' %
                        (self.corrKey, corrCfg["SF"][self.corrKey]["yearkey"], syst, wp, self.electron_syst))

                else:
                    df = df.Define("elesf_%s%s" % (wp, syst_name),
                        'corr_%s.get_ele_sf("%s", "%s", "%s", Electron_eta, Electron_pt%s, Electron_phi)' %
                        (self.corrKey, corrCfg["SF"][self.corrKey]["yearkey"], syst, wp, self.electron_syst))

                branches.append("elesf_%s%s" % (wp, syst_name))

        return df, branches


class eleSSRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(eleSSRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.prefix = kwargs.pop("runPeriod")
        self.corrKey = self.prefix + year
        self.synchedRNG = kwargs.pop("synchedRNG", False)
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        if not os.getenv("_essCorr"):
            os.environ["_essCorr"] = "_essCorr"
            if "/libCorrectionsEGM.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libCorrectionsEGM.so")
            base = "{}/src/Corrections/EGM".format(os.getenv("CMSSW_BASE"))
            ROOT.gROOT.ProcessLine(".L {}/interface/eleCorrectionsInterface.h".format(base))

        if not self.isMC:
            if not os.getenv(f"_eleSS_data_{self.corrKey}"):
                os.environ[f"_eleSS_data_{self.corrKey}"] = "_eleSS"

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_%s = eleCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameScale"]))

        else:
            if not os.getenv(f"_eleSS_mc_{self.corrKey}"):
                os.environ[f"_eleSS_mc_{self.corrKey}"] = "_eleSS"

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_smear_%s = eleCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameSmear"]))

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_scale_%s = eleCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameScale"]))

    def run(self, df):
        branches = []
        if not self.isMC:
            df = df.Define("eless_scale",
                            'corr_%s.get_ele_ss(run, Electron_seedGain, Electron_eta, Electron_r9, Electron_pt)'
                            % self.corrKey)

            df = df.Define("Electron_pt%s" % self.electron_central, "Electron_pt * eless_scale")
            df = df.Define("Electron_mass%s" % self.electron_central, "Electron_mass * eless_scale")

            branches.append("eless_scale")
            branches.append("Electron_pt%s" % self.electron_central)
            branches.append("Electron_mass%s" % self.electron_central)

        else:
            for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
                # skip all the cases for which the systematic variation would not actually be used
                if self.skip_unused_systs:
                    if self.electron_syst == self.electron_central and syst != "nom":
                        continue
                    if self.electron_syst != self.electron_central and syst != "nom" and syst_name not in self.electron_syst:
                        continue

                if self.synchedRNG:
                    df = df.Define("eless_smear_output%s" % syst_name,
                                    'corr_smear_%s.get_ele_synched_ss(event_seed, "%s", Electron_eta, Electron_r9, Electron_pt)'
                                    % (self.corrKey, syst))

                    df = df.Define("eless_smear%s" % syst_name, "eless_smear_output%s.egm_smear_factor" % syst_name)
                    df = df.Define("eless_seed%s" % syst_name, "eless_smear_output%s.egm_smear_seed" % syst_name)

                else:
                    df = df.Define("eless_smear_output%s" % syst_name,
                                    'corr_smear_%s.get_ele_ss(event, "%s", Electron_eta, Electron_r9, Electron_pt)'
                                    % (self.corrKey, syst))

                    df = df.Define("eless_smear%s" % syst_name, "eless_smear_output%s.egm_smear_factor" % syst_name)
                    df = df.Define("eless_seed%s" % syst_name, "eless_smear_output%s.egm_smear_seed" % syst_name)

                df = df.Define("Electron_pt%s%s" % (self.electron_central,syst_name),
                                "Electron_pt * eless_smear%s" % syst_name)

                df = df.Define("Electron_mass%s%s" % (self.electron_central,syst_name),
                                "Electron_mass * eless_smear%s" % syst_name)

                branches.append("eless_seed%s" % syst_name)
                branches.append("eless_smear%s" % syst_name)
                branches.append("Electron_pt%s%s" % (self.electron_central,syst_name))
                branches.append("Electron_mass%s%s" % (self.electron_central,syst_name))

                # account for the systematic arising from the scale correction on data
                if syst != "nom":
                    df = df.Define("eless_scale%s" % syst_name,
                            'corr_scale_%s.get_ele_scale_unc(run, "%s", Electron_seedGain, Electron_eta, Electron_r9, Electron_pt)'
                            % (self.corrKey, syst))

                    df = df.Define("Electron_pt%s_scale%s" % (self.electron_central,syst_name),
                                    "Electron_pt%s * eless_scale%s" % (self.electron_central,syst_name))

                    df = df.Define("Electron_mass%s_scale%s" % (self.electron_central,syst_name),
                                    "Electron_mass%s * eless_scale%s" % (self.electron_central,syst_name))

                    branches.append("eless_scale%s" % syst_name)
                    branches.append("Electron_pt%s_scale%s" % (self.electron_central,syst_name))
                    branches.append("Electron_mass%s_scale%s" % (self.electron_central,syst_name))

        return df, branches


def eleSFRDF(**kwargs):
    """
    Module to obtain electron SFs with their uncertainties.

    :param wps: name of the wps to consider among ``Loose``, ``Medium``,
        ``RecoAbove20``, ``RecoBelow20``, ``Tight``, ``Veto``, ``wp80iso`` (default),
        ``wp80noiso``, ``wp90iso,`` ``wp90noiso``.
    :type wps: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: eleSFRDF
            path: Corrections.EGM.eleCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.dataset.runPeriod
                isUL: self.dataset.has_tag('ul')
                wps: [wp80iso, ...]

    NOTE: to get the reco SFs one can just use the wps list.
        For Run3, three options are available:
            1. the three pT binned SFs are treated separately passing:  ["RecoBelow20", "Reco20to75", "RecoAbove75"]
            2. the three pT binned SFs are split into two regions:["RecoBelow20", "RecoAbove20"]
            3. the three pT binned SFs are all put together in one region: ["Reco"]
        For Run2, two options are available:
            1. the two pT binned SFs are treated separately passing:  ["RecoBelow20", "RecoAbove20"]
            3. the two pT binned SFs are all put together in one region: ["Reco"]
    """

    return lambda: eleSFRDFProducer(**kwargs)

def eleSSRDF(**kwargs):
    """
    Module to obtain electron SFs with their uncertainties.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: eleSSRDF
            path: Corrections.EGM.eleCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.dataset.runPeriod
    """

    return lambda: eleSSRDFProducer(**kwargs)
